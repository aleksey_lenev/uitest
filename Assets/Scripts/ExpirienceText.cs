﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpirienceText : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.GetComponent<Text> ().text = GetExpString ();
	}

	private string GetExpString () {
		var current = GameObject.Find ("gui_progress_bar").GetComponent<ExpirienceProgressBar> ().currentExp.ToString();
		var max = GameObject.Find ("gui_progress_bar").GetComponent<ExpirienceProgressBar> ().maxExp.ToString();
		return current + "/" + max;
	}
}
