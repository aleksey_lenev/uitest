﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class CustomButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Activate () {
		var buttonRt = this.transform.GetComponent<RectTransform> ();
		var highlightRt = GameObject.Find ("gui_ability_highlight").transform.GetComponent<RectTransform> ();

		highlightRt.localPosition = new Vector3 (buttonRt.localPosition.x - 10f, highlightRt.localPosition.y, highlightRt.localPosition.z);
	}
}
