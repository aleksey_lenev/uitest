﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpirienceProgressBar : MonoBehaviour {

	public int maxExp;
	public int currentExp;

	// Use this for initialization
	void Start () {
		SetExp ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void SetExp () {
		var bar = GameObject.Find ("gui_progress_bar").GetComponent<RectTransform>();
		var val = Map (currentExp, 0, maxExp, 0, 640);
		bar.localPosition += new Vector3 ((val - bar.sizeDelta.x) / 2, 0, 0);
		bar.sizeDelta = new Vector2 (val, bar.sizeDelta.y);
	}

	public void IncreaseExp (int amount) {
		currentExp += amount;
		if (currentExp > maxExp) {
			currentExp = maxExp;
		}
		SetExp ();
	}

	private float Map (float value, float minIn, float maxIn, float minOut, float maxOut) {
		return (value - minIn) * (maxOut - minOut) / (maxIn - minIn) + minOut;
	}
}
